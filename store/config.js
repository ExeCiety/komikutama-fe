export const state = () => ({
  appName: process.env.APP_NAME,
  appHost: process.env.APP_HOST,
  appPort: process.env.APP_PORT,
  appLocalize: process.env.APP_LOCALIZE
})

export const getters = {
  //
}

export const mutations = {
  //
}

export const actions = {
  //
}
