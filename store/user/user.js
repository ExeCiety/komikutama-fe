import routeHelper from "~/helpers/other/route-helper"
import roleHelper from "~/helpers/user/role-helper"

export const state = () => ({
  //
})

export const getters = {
  getUserLoggedInRoleName: (_state, _getters, rootState) => {
    const roles = rootState.auth.user?.roles

    return roles?.length ? roles[0].name : undefined
  },
  getUserRedirectPath: (state, getters) => {
    let redirectPath

    switch (getters.getUserLoggedInRoleName) {
      case roleHelper.roles.admin:
        redirectPath = routeHelper.dashboardPaths.admin
        break

      default:
        redirectPath = '/'
        break
    }

    return redirectPath
  }
}

export const mutations = {
  //
}

export const actions = {
  //
}
