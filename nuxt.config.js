import 'dotenv/config'
import routeHelper from './helpers/other/route-helper'

export default {
  // Environment variables
  env: {
    ...process.env
  },

  // Server
  server: {
    port: process.env.APP_PORT, // default: 3000
    host: process.env.APP_HOST, // default: localhost,
    timing: false
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: process.env.APP_NAME,
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {name: 'format-detection', content: 'telephone=no'},
      {
        name: 'title',
        hid: 'title',
        content: process.env.APP_NAME
      },
      {
        name: 'description',
        hid: 'description',
        content: process.env.APP_NAME + ' adalah platform baca komik, di mana dapat menikmati komik yang diperbaharui setiap hari secara gratis.'
      },
      {
        name: 'keywords',
        hid: 'keywords',
        content: 'platform baca komik online gratis, baca komik online gratis, baca komik online, baca komik'
      },
      {
        name: 'og:title',
        hid: 'og:title',
        content: process.env.APP_NAME
      },
      {
        name: 'og:site_name',
        hid: 'og:site_name',
        content: process.env.APP_NAME
      },
      {
        name: 'og:description',
        hid: 'og:description',
        content: process.env.APP_NAME + ' adalah platform baca komik, di mana dapat menikmati komik yang diperbaharui setiap hari secara gratis.'
      },
      {
        name: 'og:keywords',
        hid: 'og:keywords',
        content: 'platform baca komik online gratis, baca komik online gratis, baca komik online, baca komik'
      }
    ],
    link: [{rel: 'icon', type: 'image/x-icon', href: '/img/logo.ico'}]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '~/assets/scss/app.scss',
    '@fortawesome/fontawesome-svg-core/styles.css'
  ],

  styleResources: {
    scss: ['./assets/scss/_variables.scss']
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/element-ui',
    '~/plugins/fontawesome.js',
    {
      src: '~/plugins/helper.js'
    },
    {
      src: '~/plugins/service.js'
    },
    {
      src: '@/plugins/custom-directive.js',
      mode: 'client'
    },
    {
      src: '@/plugins/mixin.js',
      mode: 'client'
    },
    {
      src: '~/plugins/bootstrap',
      mode: 'client'
    },
    {
      src: '~/plugins/disqus',
      mode: 'client'
    },
    {
      src: '~/plugins/vue2-transitions',
      mode: 'client'
    },
    {
      src: '@/plugins/vue-data-tables.js',
      mode: 'client'
    }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',

    ['@nuxtjs/google-analytics', {
      id: process.env.GOOGLE_ANALYTICS_ID
    }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://image.nuxtjs.org/
    '@nuxt/image',

    '@nuxtjs/style-resources',

    '@nuxtjs/auth'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.API_HOST,
    headers: {
      common: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Accept-Language': process.env.APP_LOCALIZE
      }
    }
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: `${process.env.API_HOST}/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.public}/auth/login`,
            method: 'post',
            propertyName: 'data.login.access_token',
            headers: {
              Accept: 'application/json'
            }
          },
          logout: {
            url: `${process.env.API_HOST}/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.general}/auth/logout`,
            method: 'post',
            headers: {
              Accept: 'application/json'
            }
          },
          user: {
            url: `${process.env.API_HOST}/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.general}/users/self`,
            method: 'get',
            propertyName: 'data.user',
            headers: {
              Accept: 'application/json'
            }
          }
        },
        tokenRequired: true,
        tokenType: 'Bearer',
        user: {
          autoFetch: true
        }
      }
    },
    redirect: {
      home: '/',
      logout: '/login'
    }
  },

  loading: {
    color: '#409eff',
    height: '4px',
    throttle: 0
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/]
  },

  publicRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID
    }
  }
}
