import GlobalHelper from "~/helpers/other/global-helper";

class ComicHelper {
  statuses = [
    {
      slug: 'ongoing',
      name: 'Ongoing'
    },
    {
      slug: 'hiatus',
      name: 'Hiatus'
    },
    {
      slug: 'completed',
      name: 'Completed'
    },
    {
      slug: 'dropped',
      name: 'Dropped'
    }
  ]

  types = [
    {
      slug: 'manga',
      name: 'Manga'
    },
    {
      slug: 'manhwa',
      name: 'Manhwa'
    },
    {
      slug: 'manhua',
      name: 'Manhua'
    }
  ]

  fileNames = [
    {
      name: 'cover',
      value: 'cover'
    }
  ]

  getStatusBySlug(slug) {
    return this.statuses.filter(status => status.slug === slug)[0];
  }

  getTypeBySlug(slug) {
    return this.types.filter(type => type.slug === slug)[0];
  }

  getFileNameByName(name) {
    return this.fileNames.filter(fileName => fileName.name === name)[0]
  }

  getTagTypeByStatus(status) {
    switch (status) {
      case this.getStatusBySlug('completed')?.slug:
        return 'success';
      case this.getStatusBySlug('ongoing')?.slug:
        return 'primary';
      case this.getStatusBySlug('dropped')?.slug:
        return 'danger';
      case this.getStatusBySlug('hiatus')?.slug:
        return 'info';
      default:
        return 'info';
    }
  }

  getTagTypeByType(type) {
    switch (type) {
      case this.getTypeBySlug('manhwa')?.slug:
        return 'primary';
      case this.getTypeBySlug('manga')?.slug:
        return 'warning';
      case this.getTypeBySlug('manhua')?.slug:
        return 'danger';
      default:
        return 'info';
    }
  }

  getCoverFromFiles(files, variant = null) {
    const coverImages = files.filter(file => {
      return file.name === this.getFileNameByName('cover')?.value
    })

    const selectedCoverImage = coverImages.filter(image => {
      return variant ? image.variant === variant : true
    })[0]

    if (GlobalHelper.isBrowserSupportWebpFormat()) {
      return selectedCoverImage ? (selectedCoverImage.value + '.webp') : ''
    } else {
      return selectedCoverImage ? selectedCoverImage.value : ''
    }
  }
}

export default new ComicHelper()
