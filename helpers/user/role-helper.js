class RoleHelper {
  roles = {
    admin: 'admin',
    creator: 'creator',
    reader: 'reader'
  }

  /**
   * Get Role Label Name From User
   *
   * @param user
   * @returns {string|string}
   */
  getRoleLabelNameFromUser(user) {
    const roles = user?.roles

    return roles?.length ? this.getRoleLabelName(roles[0]?.name) : ''
  }

  /**
   * Get Role Label Name
   *
   * @param roleName
   * @returns {string}
   */
  getRoleLabelName(roleName) {
    return roleName.slice(0, 1).toUpperCase() + roleName.slice(1)
  }
}

export default new RoleHelper()
