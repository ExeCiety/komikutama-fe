class ModelHelper {
  convertMediasResponseToMediasRequest(medias) {
    return medias.filter(media => {
      return media.variant === 'original'
    }).map(media => {
      return {
        name: media.name,
        value: media.filename,
        type: media.type,
        variant: media.variant
      }
    })
  }
}

export default new ModelHelper()
