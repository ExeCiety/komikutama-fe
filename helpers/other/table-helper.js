class TableHelper {
  defaultPaginationProps = {
    background: true,
    pageSizes: [10, 20, 30, 40, 50],
    layout: 'slot, prev, pager, next',
    pageSize: 10
  }
}

export default new TableHelper()
