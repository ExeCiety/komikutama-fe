class RouteHelper {
  adminRoutes = [
    {
      name: 'dashboard',
      iconClass: 'fa-solid fa-desktop',
      path: '/admin/dashboard',
      label: 'Dashboard',
      location: ['dashboard', 'sidebar'],
      breadcrumbs: [
        {
          label: 'Dashboard',
          path: '/admin/dashboard',
          sequence: 1
        }
      ],
    },
    {
      name: 'edit-profile',
      iconClass: 'fa-solid fa-user',
      path: '/admin/profile',
      label: 'Edit Profil',
      location: ['profile', 'navbar'],
      breadcrumbs: [
        {
          label: 'Profil',
          path: '',
          sequence: 1
        },
        {
          label: 'Edit Profil',
          path: '/admin/profile',
          sequence: 2
        }
      ],
    },
    {
      name: 'edit-profile-change-password',
      iconClass: 'fa-solid fa-user',
      path: '/admin/profile/change-password',
      label: 'Ganti Password',
      location: ['profile', 'navbar'],
      breadcrumbs: [
        {
          label: 'Profil',
          path: '',
          sequence: 1
        },
        {
          label: 'Ganti Password',
          path: '/admin/profile/change-password',
          sequence: 2
        }
      ],
    },

    // Comic
    {
      name: 'manage-comic',
      iconClass: 'fa-solid fa-book',
      path: '/admin/manage-comic/list',
      label: 'Komik',
      location: ['comic', 'sidebar'],
      breadcrumbs: [
        {
          label: 'Komik',
          path: '',
          sequence: 1
        },
        {
          label: 'List',
          path: '/admin/manage-comic/list',
          sequence: 2
        }
      ],
    },
    {
      name: 'add-comic',
      iconClass: 'fa-solid fa-book',
      path: '/admin/manage-comic/add',
      label: 'Tambah Komik',
      location: ['comic'],
      breadcrumbs: [
        {
          label: 'Komik',
          path: '',
          sequence: 1
        },
        {
          label: 'Tambah',
          path: '/admin/manage-comic/add',
          sequence: 2
        }
      ],
    },
    {
      name: 'edit-comic',
      iconClass: 'fa-solid fa-book',
      path: '/admin/manage-comic/edit',
      label: 'Edit Komik',
      location: ['comic'],
      breadcrumbs: [
        {
          label: 'Komik',
          path: '',
          sequence: 1
        },
        {
          label: 'Edit',
          path: '/admin/manage-comic/edit',
          sequence: 2
        }
      ],
    },

    // Genre
    {
      name: 'manage-genre',
      iconClass: 'fa-solid fa-masks-theater',
      path: '/admin/manage-genre/list',
      label: 'Genre',
      location: ['genre', 'sidebar'],
      breadcrumbs: [
        {
          label: 'Genre',
          path: '',
          sequence: 1
        },
        {
          label: 'List',
          path: '/admin/manage-genre/list',
          sequence: 2
        }
      ],
    },
    {
      name: 'add-genre',
      iconClass: 'fa-solid fa-masks-theater',
      path: '/admin/manage-genre/add',
      label: 'Tambah Genre',
      location: ['genre'],
      breadcrumbs: [
        {
          label: 'Genre',
          path: '',
          sequence: 1
        },
        {
          label: 'Tambah',
          path: '/admin/manage-genre/add',
          sequence: 2
        }
      ],
    },
    {
      name: 'edit-genre',
      iconClass: 'fa-solid fa-masks-theater',
      path: '/admin/manage-genre/edit',
      label: 'Edit Genre',
      location: ['genre'],
      breadcrumbs: [
        {
          label: 'Genre',
          path: '',
          sequence: 1
        },
        {
          label: 'Edit',
          path: '/admin/manage-genre/edit',
          sequence: 2
        }
      ],
    },

    // Author
    {
      name: 'manage-author',
      iconClass: 'fa-solid fa-user-tag',
      path: '/admin/manage-author/list',
      label: 'Author',
      location: ['author', 'sidebar'],
      breadcrumbs: [
        {
          label: 'Author',
          path: '',
          sequence: 1
        },
        {
          label: 'List',
          path: '/admin/manage-author/list',
          sequence: 2
        }
      ],
    },
    {
      name: 'add-author',
      iconClass: 'fa-solid fa-user-tag',
      path: '/admin/manage-author/add',
      label: 'Tambah Author',
      location: ['user'],
      breadcrumbs: [
        {
          label: 'Author',
          path: '',
          sequence: 1
        },
        {
          label: 'Tambah',
          path: '/admin/manage-author/add',
          sequence: 2
        }
      ],
    },
    {
      name: 'edit-author',
      iconClass: 'fa-solid fa-user-tag',
      path: '/admin/manage-author/edit',
      label: 'Edit Author',
      location: ['user'],
      breadcrumbs: [
        {
          label: 'Author',
          path: '',
          sequence: 1
        },
        {
          label: 'Edit',
          path: '/admin/manage-author/edit',
          sequence: 2
        }
      ],
    },

    // Artist
    {
      name: 'manage-artist',
      iconClass: 'fa-solid fa-user-pen',
      path: '/admin/manage-artist/list',
      label: 'Artist',
      location: ['artist', 'sidebar'],
      breadcrumbs: [
        {
          label: 'Artist',
          path: '',
          sequence: 1
        },
        {
          label: 'List',
          path: '/admin/manage-artist/list',
          sequence: 2
        }
      ],
    },
    {
      name: 'add-artist',
      iconClass: 'fa-solid fa-user-pen',
      path: '/admin/manage-artist/add',
      label: 'Tambah Artist',
      location: ['user'],
      breadcrumbs: [
        {
          label: 'Artist',
          path: '',
          sequence: 1
        },
        {
          label: 'Tambah',
          path: '/admin/manage-artist/add',
          sequence: 2
        }
      ],
    },
    {
      name: 'edit-artist',
      iconClass: 'fa-solid fa-user-pen',
      path: '/admin/manage-artist/edit',
      label: 'Edit Artist',
      location: ['user'],
      breadcrumbs: [
        {
          label: 'Artist',
          path: '',
          sequence: 1
        },
        {
          label: 'Edit',
          path: '/admin/manage-artist/edit',
          sequence: 2
        }
      ],
    },

    // User
    {
      name: 'manage-user',
      iconClass: 'fa-solid fa-user',
      path: '/admin/manage-user/list',
      label: 'Pengguna',
      location: ['user', 'sidebar'],
      breadcrumbs: [
        {
          label: 'Pengguna',
          path: '',
          sequence: 1
        },
        {
          label: 'List',
          path: '/admin/manage-user/list',
          sequence: 2
        }
      ],
    },
    {
      name: 'add-user',
      iconClass: 'fa-solid fa-user',
      path: '/admin/manage-user/add',
      label: 'Tambah Pengguna',
      location: ['user'],
      breadcrumbs: [
        {
          label: 'Pengguna',
          path: '',
          sequence: 1
        },
        {
          label: 'Tambah',
          path: '/admin/manage-user/add',
          sequence: 2
        }
      ],
    },
    {
      name: 'edit-user',
      iconClass: 'fa-solid fa-user',
      path: '/admin/manage-user/edit',
      label: 'Edit Pengguna',
      location: ['user'],
      breadcrumbs: [
        {
          label: 'Pengguna',
          path: '',
          sequence: 1
        },
        {
          label: 'Edit',
          path: '/admin/manage-user/edit',
          sequence: 2
        }
      ],
    },
  ]

  creatorRoutes = [
    {
      name: 'dashboard',
      iconClass: 'fa-solid fa-desktop',
      path: '/creator/dashboard',
      label: 'Dashboard',
      location: ['dashboard', 'sidebar'],
    }
  ]

  readerRoutes = [
    {
      name: 'dashboard',
      iconClass: 'fa-solid fa-desktop',
      path: '/reader/dashboard',
      label: 'Dashboard',
      location: ['dashboard', 'sidebar'],
    }
  ]

  dashboardPaths = {
    admin: this.filterRoutesByLocation(this.adminRoutes, 'dashboard')[0].path,
    creator: this.filterRoutesByLocation(this.creatorRoutes, 'dashboard')[0].path,
    reader: this.filterRoutesByLocation(this.readerRoutes, 'dashboard')[0].path
  }

  adminSidebarItems = this.filterRoutesByLocation(this.adminRoutes, 'sidebar')

  creatorSidebarItems = this.filterRoutesByLocation(this.creatorRoutes, 'sidebar')

  readerSidebarItems = this.filterRoutesByLocation(this.readerRoutes, 'sidebar')

  pathPrefixes = {
    v1: 'v1',
    public: 'public',
    general: 'general',
    admin: 'admin',
    creator: 'creator',
    reader: 'reader',
  }

  uploadRouteData = {
    url: `${process.env.API_HOST}/${this.pathPrefixes.v1}/${this.pathPrefixes.general}/files/upload`,
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  }

  /**
   * Filter Routes By Location
   *
   * @param routes
   * @param location
   * @returns {*}
   */
  filterRoutesByLocation(routes, location) {
    return routes?.filter((route) => {
      return route.location.includes(location)
    })
  }

  /**
   * Filter Routes By Location
   *
   * @param routes
   * @param name
   * @returns {*}
   */
  filterRoutesByName(routes, name) {
    return routes?.filter((route) => {
      return route.name === name
    })
  }

}

export default new RouteHelper()
