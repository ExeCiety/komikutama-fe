import axios from 'axios'
import nProgress from "nprogress";

const apiClient = axios.create({
  baseURL: process.env.API_HOST,
  withCredentials: false,
  timeout: 15000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Accept-Language': process.env.APP_LOCALIZE
  }
})

apiClient.interceptors.request.use((config) => {
  if (config.loading === true) {
    nProgress.start()
  }

  return config
}, (error) => {
  nProgress.done()
  return Promise.reject(error)
})

apiClient.interceptors.response.use((response) => {
  nProgress.done()
  return response
}, (error) => {
  nProgress.done()
  return Promise.reject(error)
})

export {apiClient}
