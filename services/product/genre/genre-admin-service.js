import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Get Genres For Admin
   *
   * @param payload
   * @returns {*}
   */
  getGenres(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/genres`,
      params: payload?.params
    })
  },

  /**
   * Get Genres For Admin
   *
   * @param payload
   * @returns {*}
   */
  getGenre(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/genres/${payload?.id}`
    })
  },

  /**
   * Add Genre For Admin
   * @param payload
   * @returns {*}
   */
  addGenre(payload) {
    return ctx.$axios({
      method: 'post',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/genres`,
      data: payload?.data
    })
  },

  /**
   * Add Genre For Admin
   * @param payload
   * @returns {*}
   */
  updateGenre(payload) {
    return ctx.$axios({
      method: 'put',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/genres/${payload?.id}`,
      data: payload?.data
    })
  },

  /**
   * Delete Genres For Admin
   *
   * @param payload
   * @returns {*}
   */
  deleteGenres(payload) {
    return ctx.$axios({
      method: 'delete',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/genres`,
      data: payload?.data
    })
  }
})
