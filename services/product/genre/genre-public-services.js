import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Get Genres For Admin
   *
   * @param payload
   * @returns {*}
   */
  getGenres(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.public}/genres`,
      params: payload?.params
    })
  },
})
