import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Get Comics For Admin
   *
   * @param payload
   * @returns {*}
   */
  getComics(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/comics`,
      params: payload?.params
    })
  },

  /**
   * Get Comics For Admin
   *
   * @param payload
   * @returns {*}
   */
  getComic(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/comics/${payload?.id}`
    })
  },

  /**
   * Add Comic For Admin
   * @param payload
   * @returns {*}
   */
  addComic(payload) {
    return ctx.$axios({
      method: 'post',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/comics`,
      data: payload?.data
    })
  },

  /**
   * Add Comic For Admin
   * @param payload
   * @returns {*}
   */
  updateComic(payload) {
    return ctx.$axios({
      method: 'put',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/comics/${payload?.id}`,
      data: payload?.data
    })
  },

  /**
   * Delete Comics For Admin
   *
   * @param payload
   * @returns {*}
   */
  deleteComics(payload) {
    return ctx.$axios({
      method: 'delete',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/comics`,
      data: payload?.data
    })
  }
})
