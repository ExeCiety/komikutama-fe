import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Upload Files
   *
   * @param payload
   * @returns {*}
   */
  uploadFiles(payload) {
    return ctx.$axios({
      method: 'post',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.general}/files/upload`,
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: payload.data
    })
  }
})
