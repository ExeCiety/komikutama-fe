import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Get Authors For Admin
   *
   * @param payload
   * @returns {*}
   */
  getAuthors(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/authors`,
      params: payload?.params
    })
  },

  /**
   * Get Authors For Admin
   *
   * @param payload
   * @returns {*}
   */
  getAuthor(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/authors/${payload?.id}`
    })
  },

  /**
   * Add Author For Admin
   * @param payload
   * @returns {*}
   */
  addAuthor(payload) {
    return ctx.$axios({
      method: 'post',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/authors`,
      data: payload?.data
    })
  },

  /**
   * Add Author For Admin
   * @param payload
   * @returns {*}
   */
  updateAuthor(payload) {
    return ctx.$axios({
      method: 'put',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/authors/${payload?.id}`,
      data: payload?.data
    })
  },

  /**
   * Delete Authors For Admin
   *
   * @param payload
   * @returns {*}
   */
  deleteAuthors(payload) {
    return ctx.$axios({
      method: 'delete',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/authors`,
      data: payload?.data
    })
  }
})
