import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Get Users For Admin
   *
   * @param payload
   * @returns {*}
   */
  getUsers(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/users`,
      params: payload?.params
    })
  },

  /**
   * Get Users For Admin
   *
   * @param payload
   * @returns {*}
   */
  getUser(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/users/${payload?.id}`
    })
  },

  /**
   * Add User For Admin
   * @param payload
   * @returns {*}
   */
  addUser(payload) {
    return ctx.$axios({
      method: 'post',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/users`,
      data: payload?.data
    })
  },

  /**
   * Add User For Admin
   * @param payload
   * @returns {*}
   */
  updateUser(payload) {
    return ctx.$axios({
      method: 'put',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/users/${payload?.id}`,
      data: payload?.data
    })
  },

  /**
   * Delete Users For Admin
   *
   * @param payload
   * @returns {*}
   */
  deleteUsers(payload) {
    return ctx.$axios({
      method: 'delete',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/users`,
      data: payload?.data
    })
  }
})
