import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Update User Logged In
   *
   * @param payload
   * @returns {*}
   */
  updateUserLoggedIn(payload) {
    return ctx.$axios({
      method: 'put',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.general}/users/self`,
      data: payload?.data
    })
  },

  /**
   * Change Password User Logged In
   *
   * @param payload
   * @returns {*}
   */
  changePasswordUserLoggedIn(payload) {
    return ctx.$axios({
      method: 'put',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.general}/users/self/change-password`,
      data: payload?.data
    })
  }
})
