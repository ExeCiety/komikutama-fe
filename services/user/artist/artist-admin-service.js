import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Get Artists For Admin
   *
   * @param payload
   * @returns {*}
   */
  getArtists(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/artists`,
      params: payload?.params
    })
  },

  /**
   * Get Artists For Admin
   *
   * @param payload
   * @returns {*}
   */
  getArtist(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/artists/${payload?.id}`
    })
  },

  /**
   * Add Artist For Admin
   * @param payload
   * @returns {*}
   */
  addArtist(payload) {
    return ctx.$axios({
      method: 'post',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/artists`,
      data: payload?.data
    })
  },

  /**
   * Add Artist For Admin
   * @param payload
   * @returns {*}
   */
  updateArtist(payload) {
    return ctx.$axios({
      method: 'put',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/artists/${payload?.id}`,
      data: payload?.data
    })
  },

  /**
   * Delete Artists For Admin
   *
   * @param payload
   * @returns {*}
   */
  deleteArtists(payload) {
    return ctx.$axios({
      method: 'delete',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/artists`,
      data: payload?.data
    })
  }
})
