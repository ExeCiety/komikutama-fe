import routeHelper from "~/helpers/other/route-helper";

export default (ctx) => () => ({
  /**
   * Get Roles For Admin
   *
   * @param payload
   * @returns {*}
   */
  getRoles(payload) {
    return ctx.$axios({
      method: 'get',
      url: `/${routeHelper.pathPrefixes.v1}/${routeHelper.pathPrefixes.admin}/roles`,
      params: payload?.params
    })
  }
})
