export default {
  methods: {
    setToQuery(query) {
      this.$router.push({
        query: {
          ...this.$route.query,
          ...query
        }
      })
    }
  }
}
