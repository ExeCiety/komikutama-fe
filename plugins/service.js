import userGeneralService from "~/services/user/user/user-general-service";
import userAdminService from "~/services/user/user/user-admin-service";
import roleAdminService from "~/services/user/role/role-admin-service";
import genreAdminService from "~/services/product/genre/genre-admin-service";
import genrePublicServices from "~/services/product/genre/genre-public-services";
import authorAdminService from "~/services/user/author/author-admin-service";
import artistAdminService from "~/services/user/artist/artist-admin-service";
import comicAdminService from "~/services/product/comic/comic-admin-service";
import fileGeneralService from "~/services/other/file/file-general-service";

export default function (ctx, inject) {
  inject('services', {
    // User
    // User
    userGeneral: userGeneralService(ctx),
    userAdmin: userAdminService(ctx),

    // Role
    roleAdmin: roleAdminService(ctx),

    // Author
    authorAdmin: authorAdminService(ctx),

    // Artist
    artistAdmin: artistAdminService(ctx),

    // Product
    // Comic
    comicAdmin: comicAdminService(ctx),

    // Genre
    genreAdmin: genreAdminService(ctx),
    genrePublic: genrePublicServices(ctx),

    // Other
    fileGeneral: fileGeneralService(ctx),
  })
}
