import roleHelper from '~/helpers/user/role-helper';
import routeHelper from '~/helpers/other/route-helper';
import tableHelper from "~/helpers/other/table-helper";
import stringHelper from "~/helpers/other/string-helper";
import comicHelper from "~/helpers/product/comic-helper";
import modelHelper from "~/helpers/other/model-helper";

export default function (ctx, inject) {
  inject('helpers', {
    // User
    role: roleHelper,

    // Product
    comic: comicHelper,

    // Other
    route: routeHelper,
    table: tableHelper,
    string: stringHelper,
    model: modelHelper,
  })
}
