import Vue from 'vue'
// eslint-disable-next-line import/default,no-unused-vars
import boostrap, {Modal} from 'bootstrap'

Vue.mixin({
  methods: {
    getBootstrapModal(id) {
      return new Modal(id)
    }
  }
})
