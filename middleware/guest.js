export default function ({store, $auth, redirect}) {
  if ($auth.loggedIn) {
    return redirect(store.getters['user/user/getUserRedirectPath'])
  }
}
