export default function ({store, redirect}) {
  if (store.getters['user/user/getUserLoggedInRoleName'] !== 'admin') {
    return redirect('/login')
  }
}
